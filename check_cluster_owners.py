import requests, json, time

def get_owner(address):

    #print("Getting owner of: " + address)
    temp = ""
    count = 0
    owner_list = ""

    while (count < 5):

        try:
            url = requests.get("https://api.helium.io/v1/hotspots/" + address)
            owner_list = json.loads(url.text)
            owner_list = owner_list["data"]
            count = 5
        except:
            print(address)
            print(url)
            print(url.text)
            if "come_back_in_ms" in owner_list:
                print("Waiting accordingly")
                time.sleep(int(owner_list["come_back_in_ms"] / 1000))
            else:
                time.sleep(5)
            count = count + 1

    if "owner" in owner_list:
        temp = owner_list["owner"]
    else:
        print("Couldn't find owner for " + address)
    #print("Owner found: " + temp)
    return temp

def get_witnesses(address):

    count = 0
    witness_list = ""

    while (count < 5):
        
        try:
            url = requests.get("https://api.helium.io/v1/hotspots/" + address + "/witnesses/")
            witness_list = json.loads(url.text)
            witness_list = witness_list["data"]    
            count = 5
        except:
            print(address)
            print(url)
            print(url.text)
            if "come_back_in_ms" in witness_list:
                print("Waiting accordingly")
                time.sleep(int(witness_list["come_back_in_ms"] / 1000))
            else:
                time.sleep(5)
            count = count + 1

    witnesses = []

    for witness in witness_list:
        #print(witness)
        witnesses.extend([witness["address"]])

    return witnesses

def get_hotspots(address):

    count = 0
    hotspot_list = ""

    while (count < 5):
        try:
            url = requests.get("https://api.helium.io/v1/accounts/" + address + "/hotspots/")
            hotspot_list = json.loads(url.text)
            hotspot_list = hotspot_list["data"]
            count = 5
        except:
            print(address)
            print(url)
            print(url.text)
            if "come_back_in_ms" in hotspot_list:
                print("Waiting accordingly")
                time.sleep(int(hotspot_list["come_back_in_ms"] / 1000))
            else:
                time.sleep(5)
            count = count + 1

    hotspots = []

    for hotspot in hotspot_list:
        hotspots.extend([hotspot["address"]])

    return hotspots

count = 0
visited = []
def iterate_witnesses(owner_index, owners, extra_witnesses):
    count = 0

    first_hotspots = get_hotspots(owners[owner_index])
    for hotspot in first_hotspots:
        if hotspot not in visited:
            print(hotspot + ",")
            count = count + 1

            time.sleep(2)
            first_witnesses = get_witnesses(hotspot)
            for witness in first_witnesses:
                if witness not in visited:
                    print(witness + ",")
                    count = count + 1
                    visited.extend([witness])
                    time.sleep(2)
                    temp_owner = get_owner(witness)
                    if temp_owner not in owners:
                        owners.extend([temp_owner])
            visited.extend([hotspot])

    return owners


#two hotspots to check - I just put in two randomly for this example
owner = "14TpHwxGWgKcAmEMPRQWTWRQzErwNEMQzMet4CGpEMZ2s7rM6iy" #panther cluster
owners = []
extra_witnesses = []
owners.extend(["14TpHwxGWgKcAmEMPRQWTWRQzErwNEMQzMet4CGpEMZ2s7rM6iy"]) #cluster witnessing to another wallet
owners.extend(["14bMgYMLLVPkmSVekj4Gnqp6HRSXkNZ2aZ8FuT7b3P7N1rBniFx"]) #isolated panther cluster
owner_index = 0

while owner_index < len(owners) and owner_index < 5: #don't tax the API
    owners = iterate_witnesses(owner_index, owners, extra_witnesses)
    owner_index = owner_index + 1
    
print()
print("Total found: " + str(count))
print("Other owners to check: ")
for owner in owners:
    print(owner)
