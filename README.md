# Code Snippets

## Name
Little tidbits and snippets to understand what's going on with the Helium Network.

## Description
* check_overlap.py - identifies hotspots that overlap witnesses in their coverage then looks at that count vs the total number of witnesses for that hotspot. Two examples are provided. At the time of initial coding, it appears like these to hotspots were overlapping between ~50-70%.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals


## Installation
Install latest Python.

## Usage
Please don't use this to doxx others, it's just for research purposes and making the network better.

* For check_overlap.py - Once you've cloned the repo, prime with hotspot addresses, use `python check_overlap.py` to run the code.
* For check_cluster_owners.py - Once you've cloned the repo, prime with a few owner addresses, use `python check_cluster_owners.py` to run the code.

## Support
Feel free to create an issue in the project if you'd like to see something.

## Roadmap
If you have ideas for releases in the future, put some in issues.

## Contributing
MRs are welcome. If you do make substantial changes, please document the code and update the readme. We should document with a self-starter in mind.

## Authors and acknowledgment
If you find value in this code, you can contribute to #ThePeoplesHotspot HNT wallet (14sDZPutwnEatxrUhjrSZCXAN6EzYWaD2ZBUBbhNmaL1LZcvZcK) or tip me via Twitter: https://twitter.com/EdBallou.

## License
MIT License. Just treat this like stack overflow.

## Project status
When time/interest permits, things could be added.

